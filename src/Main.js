import React, { Component } from 'react';
import { Route, NavLink, HashRouter, Link } from 'react-router-dom';
import { isAbsolute } from 'path';
import Home from './views/Home';
import Contact from './views/Contact';
import About from './views/About';
import './styles/main.css';


export default class Main extends Component {
    render() {
        return (
            <HashRouter>

                <div>
                    <h1 className="title">React First Application</h1>
                    <ul className="header">
                        <li><NavLink to="/">Home</NavLink></li>
                        <li><NavLink to="/about">About</NavLink></li>
                        <li><NavLink to="/contact">Contact</NavLink></li>
                    </ul>
                    <div>
                        <Route exact path="/" component={Home} />
                        <Route exact path="/about" component={About} />
                        <Route exact path="/contact" component={Contact} />
                    </div>
                </div>
            </HashRouter>
        )
    }
}